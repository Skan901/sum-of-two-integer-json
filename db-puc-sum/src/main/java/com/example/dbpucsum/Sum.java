package com.example.dbpucsum;


public class Sum {

    private Integer firstNumber;
    private Integer secondNumber;
    private Integer sumOfNumbers;

	public Sum(){
	
	}

    public Sum (Integer firstNumber,Integer  secondNumber){
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.sumOfNumbers = firstNumber + secondNumber;

    }

    public Integer getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(Integer firstNumber) {
        this.firstNumber = firstNumber;
    }

    public Integer getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(Integer secondNumber) {
        this.secondNumber = secondNumber;
    }

    public Integer getSumOfNumbers() {
    	return sumOfNumbers;
    }


}