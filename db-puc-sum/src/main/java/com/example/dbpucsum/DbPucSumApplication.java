package com.example.dbpucsum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbPucSumApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbPucSumApplication.class, args);
	}

}
