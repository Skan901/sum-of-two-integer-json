package com.example.dbpucsum;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sum")
public class SumController {


    @GetMapping
    public Sum getSum(Sum s) {
        return new Sum(s.getFirstNumber(),
                s.getSecondNumber());
    }


}

