# Sum of Two Integers JSON (Spring Boot with Gradle) / Soma de Dois Inteiros JSON (Spring Boot com Gradle)

[eng]

The application was devoloped as a homework for the onbarding at the instership for the company [DBServer](https://db.tec.br/) onboarding with professor [Marco Aurelio Souza Mangan](http://lattes.cnpq.br/7555399269710671) in his Java/Spring class during our first month taking syncronous online classes at [PUC-RS](https://tecnopuc.pucrs.br/).

The task was to develop a aplication that would get two integer numbers directly from the broweser URL (we could use Postman), make a sum out of this two values and return a JSON file with the three values: 1st Integer, 2nd Integer and the Sum of those Integers. E.g.:

(URL) http://localhost:8080/sum?firstNumber=110&secondNumber=20

(JSON) {"firstNumber":110,"secondNumber":20,"sumOfNumbers":130}

[pt-br]

A aplicação foi desenvolvido como lição de casa para o estágio de integração da empresa [DBServer](https://db.tec.br/) com o professor [Marco Aurelio Souza Mangan](http://lattes.cnpq.br/7555399269710671) em sua aula de Java/Spring durante nosso primeiro mês de aulas síncronas online na [PUC-RS](https://tecnopuc.pucrs.br/).

A tarefa era desenvolver uma aplicação que pegasse dois números inteiros diretamente da URL do navegador (poderíamos usar o Postman), fazer uma soma desses dois valores e retornar um arquivo JSON com os três valores: 1º Inteiro, 2º Inteiro e a Soma desses inteiros. Por exemplo:

(URL) http://localhost:8080/sum?firstNumber=110&secondNumber=20

(JSON) {"firstNumber":110,"secondNumber":20,"sumOfNumbers":130}

## Basic Environment Setup / Configuração Básica do Ambiente

[eng]


- [ ] For this project it's necessary to have the JDK installed. I'll be using Java 17 LTS for this. You can just [click here](https://www.oracle.com/java/technologies/downloads/#java17) and go to the website to download it. You also need to add the necessary path variables in your system for it to work.
- [ ] It's also necessary to have Gradle installed in your machine, you can [click here](https://gradle.org/install/) to find the information on how to download it and it's also necessary to have the path variables set in your system as well.
- [ ] I'll be using Eclipse IDE for Java Developers (my version is the 2021-12 v4.22.0), you can find a newer version for your system by [clicking here](https://www.eclipse.org/downloads/packages/) and it should work the same as well.

[pt-br]


- [ ] Para este projeto é necessário ter o JDK instalado. Eu estarei usando o Java 17 LTS. Você pode apenas [clicar aqui](https://www.oracle.com/java/technologies/downloads/#java17) e acessar o site para baixá-lo. Você também precisa adicionar o caminho das variáveis em seu sistema para que ele funcione.
- [ ] Também é necessário ter o Gradle instalado em sua máquina, você pode [clicar aqui](https://gradle.org/install/) para encontrar as informações de como baixá-lo e também é necessário ter o caminho das variáveis em seu sistema também definidas.
- [ ] Estarei usando o Eclipse IDE para desenvolvedores Java (minha versão é a 2021-12 (4.22.0)), você pode encontrar uma versão mais recente para o seu sistema [clicando aqui](https://www.eclipse.org/downloads/packages/) e deve funcionar da mesma forma.


## Eclipse Setup for Running Spring Boot / Configurações Extra do Eclipse Para Executar Spring Boot

[eng]

So you can natively run and debug Spring boot projects from Eclipse, you'll need to follow a few extra steps. You could also download the [Spring Tool Suite 4](https://spring.io/tools), but since I already had Eclipse installed in my machine, I'll just add the follow procedure for those like me who just want to add the support for Spring Boot.

- [ ] First open up Eclipse and go to *Help* and open the *Eclipse mmarketplace...*

![image.png](./image.png)

- [ ] Look up in the *find* bar for *spring* or *sts* (Spring Tool Suite), find the *Spring Tools 4 (aka Spring Tool Suite 4)* and click in *install*.

![image-1.png](./image-1.png)

- [ ] Mark all the options, and *Confirm >*, after you'll *"Accept or Approve"* any license that appears and the end click *Finish*. After that you'll should restart the appplication for the changes to take effect.

![image-2.png](./image-2.png)

- [ ] Since we are working with Gradle, you'll also need the *Buildship Gradle Integration*, so search for *Gradle* in the same marketplace, and install it. After that you'll should also restart the appplication for the changes to take effect.

![image-3.png](./image-3.png)

I'll be adding extra step for build the project in the Windows prompt command latter in this README, but as of now you are good to run your projects directly from the IDE.

[pt-br]

Para que você possa executar e fazer o debug nativamente dos projetos do Spring Boot no Eclipse, será necessário seguir algumas etapas extras. Você também pode baixar o [Spring Tool Suite 4](https://spring.io/tools), mas como eu já tinha o Eclipse instalado na minha máquina, vou apenas fazer os seguintes procedimentos para aquele como eu que querem apenas adicionar o suporte ao Spring Boot.

- [ ] Primeiro abra o Eclipse e vá para *Help* e abra o *Eclipse mmarketplace...*

![image.png](./image.png)

- [ ] Procure na barra *find* por *spring* ou *sts* (Spring Tool Suite), encontre o *Spring Tools 4 (também conhecido como Spring Tool Suite 4)* e clique em *install*.

![image-1.png](./image-1.png)

- [ ] Marque todas as opções, e *Confirme >*, após você irá *"Aceitar ou Aprovar"* qualquer licença que apareça e ao final clicar em *Concluir* (Finish). Depois disso, você deverá reiniciar o aplicativo para que as alterações tenham efeito.

![image-2.png](./image-2.png)

- [ ] Como estamos trabalhando com Gradle, você também precisará da *Buildship Gradle Integration*, então procure por *Gradle* no mesmo *marketplace* e instale-o. Depois você também deve reiniciar o aplicativo para que as alterações tenham efeito.

![image-3.png](./image-3.png)

Eu estarei adicionando uma etapa extra para construir (build) o projeto no comando do prompt do Windows neste README, mas a partir de agora você está pronto para executar seus projetos direto da IDE.



## Setting up the Spring Initializr / Configurando o Spring Initializr

[eng]

I'll be using the [Spring Initializr](https://start.spring.io/) for this project. I'll be naming it *db-puc-sum*, but you can name it whatever you want. I'll be adding (even if I'm not using all of then right now) the following *Dependencies*: 

- [ ] Spring Web
- [ ] Spring Boot DevTools
- [ ] Spring Boot Actuator
- [ ] Spring Security 

I'm also going to choose for this Java 17 and Gradle. You could also add *Spring Data JPA* for working with SQL, but I don't feel that's necessary that's why I won't be adding it to the project.

You can make the setup yourself like the image bellow, or you can just download the shared version I made by [clicking here](https://start.spring.io/#!type=gradle-project&language=java&platformVersion=2.7.4&packaging=jar&jvmVersion=17&groupId=com.example&artifactId=db-puc-sum&name=db-puc-sum&description=Demo%20project%20for%20Spring%20Boot&packageName=com.example.db-puc-sum&dependencies=web,devtools,actuator,security)

![image-4.png](./image-4.png)

[pt-br]

Usarei o [Spring Initializr](https://start.spring.io/) para este projeto. Vou nomeá-lo *db-puc-sum*, mas você pode nomear como quiser. Estarei adicionando (mesmo que não esteja usando tudo isso agora) as seguintes *Dependências*:

- [ ] Spring Web
- [ ] Spring Boot DevTools
- [ ] Spring Boot Actuator
- [ ] Spring Security 

Também vou optar pelo Java 17 e Gradle. Você também pode adicionar *Spring Data JPA* para trabalhar com SQL, mas não acho necessário, por isso não o adicionarei ao projeto.

Você mesmo pode fazer a configuração como na imagem abaixo, ou pode simplesmente baixar a versão compartilhada que fiz [clicando aqui](https://start.spring.io/#!type=gradle-project&language=java&platformVersion=2.7.4&packaging=jar&jvmVersion=17&groupId=com.example&artifactId=db-puc-sum&name=db-puc-sum&description=Demo%20project%20for%20Spring%20Boot&packageName=com.example.db-puc-sum&dependencies=web,devtools,actuator,security)

![image-4.png](./image-4.png)

## How to open the Spring Boot (Gradle) Project in Eclipse / Como abrir o projeto Spring Boot (Gradle) no Eclipse

[eng]

- [ ] First, after you downloaded the Initializr project, unzip it in your computer where you can find it easily. After that you need to open Eclipse and go to *File -> Import*. 

![image-5.png](./image-5.png)

- [ ] In the window, you'll choose the folder *Gradle* and inside that the option *Existing Gradle Project*. 

![image-6.png](./image-6.png)

- [ ] After that you'll be prompted with a "Welcome Page", you can uncheck the box there if don't want this to show the next time you import a Gradle Project. After that, just click *Next*.

![image-7.png](./image-7.png)

- [ ] At this window you'll click in *Browse* and choose the folder of the project you've unzipped, and after that just click *Finish*. If everything goes well, the project will be added to the *Project Explorer* menu at the left upper corner of Eclipse. 

![image-8.png](./image-8.png)

NOTE: If you don't see the *Project Explorer*, just go at the bar menu to *Window -> Show View -> Project Explorer* and it will appear.

![image-9.png](./image-9.png)

[pt-br]

- [ ] Primeiro, depois de baixar o projeto Spring Initializr, descompacte-o em seu computador, onde você podssa encontrá-lo facilmente. Depois disso, você precisa abrir o Eclipse e ir para *File -> Import*.

![image-5.png](./image-5.png)

- [ ] Na janela, você escolherá a pasta *Gradle* e dentro dela a opção *Existing Gradle Project*.

![image-6.png](./image-6.png)

- [ ] Depois disso, será exibida uma "Página de boas-vindas", você pode desmarcar a caixa se não quiser que isso seja exibido na próxima vez que você importar um projeto Gradle. Depois, basta clicar em *Avançar*.

![image-7.png](./image-7.png)

- [ ] Nesta janela você clicará em *Browse* e escolherá a pasta do projeto que você descompactou, e depois disso é só clicar em *Finish*. Se tudo correr bem, o projeto será adicionado ao menu *Project Explorer* no canto superior esquerdo do Eclipse.

![image-8.png](./image-8.png)

NOTA: Se você não estiver vendo o *Project Explorer*, basta entrar no menu na barra superior em *Window -> Show View -> Project Explorer* e ele aparecerá.

![image-9.png](./image-9.png)

